@extends('layouts.app')

@section('content')

<div class="grid grid-cols-1 m-auto background-image">

    <div class="flex justify-center items-center text-gray-100 pt-10">

        <div class="sm:m-auto w-4/5 sm:pt-2 lg:pt-4 pb-16 block text-center">

            <h1 class="lg:text-5xl text-2xl pb-14 uppercase font-bold  sm:text-white text-shadow-md">
                Do you want to become a developper ?
            </h1>

            <a href="/blog" class="text-center bg-gray-50 font-bold py-4 px-3 text-gray-700
            text-xl uppercase">Read More</a>

        </div>
    
    </div>

</div>

<div class="sm:grid grid-cols-2 gap-20 w-4/5 py-5 mx-auto border-b border-gray-200 ">

    <div>

        <img src="https://cdn.pixabay.com/photo/2014/12/15/14/05/home-office-569153_960_720.jpg" alt="laptop image" >

    </div>

    <div class="text-left w-4/5 block m-auto sm:m-auto">

        <h2 class="text-4xl text-gray-600 font-extrabold">
            Struggling to be a web developper.
        </h2>

        <p class="text-gray-500 py-8 text-sm">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
            Inventore animi, neque eum, porro minima ducimus assumenda modi 
        </p>

        <p class="text-gray-500 font-extrabold mb-10 text-xl">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
            Inventore animi, neque eum, porro minima ducimus assumenda modi 
        </p>
        <a href="/blog" class="rounded-3xl py-3 px-8 uppercase bg-blue-500 text-gray-300
        font-extrabold ">
            Find More
        </a>

    </div>

</div>

<div class="bg-black text-white text-center p-15">

    <h2 class="text-2xl pb-5 ">
        I am an expert in...
    </h2>

    <span class="block text-4xl font-extrabold py-1">
        UX Design
    </span>

    <span class="block text-4xl font-extrabold py-1">
        Project Management
    </span>

    <span class="block text-4xl font-extrabold py-1">
        Digital Strategy
    </span>

    <span class="block text-4xl font-extrabold py-1">
        Backend Developpement
    </span>


</div>

<div class="text-center py-15">

    <span class="uppercase text-sm text-gray-400 border-b-2 border-gray-200">
        Blog
    </span>

    <h2 class="text-4xl py-10 font-bold ">

        Recent post
    </h2>

    <p class="text-gray-600 py-3 w-4/5 m-auto">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. 
        Quos, rerum deserunt aut nihil sed soluta in eos quam 
        asperiores ratione adipisci quaerat dicta 
        aliquam excepturi tenetur rem suscipit esse commodi.
    </p>
</div>

<div class="m-auto w-4/5 sm:grid grid-cols-2 ">

    <div class="flex  bg-yellow-700 pt-10 text-gray-100">

        <div class="sm:m-auto pt-4 pb-16 w-4/5 block">

            <span class="text-xs uppercase">PHP</span>

            <h3 class="text-xl py-10 font-bold">
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
                Ducimus deserunt atque deleniti mollitia temporibus possimus 
                pariatur ratione nemo, aspernatur neque iusto 
                distinctio facere doloremque? Tempore commodi enim atque aspernatur vero.
            </h3>

            <a href="" class="uppercase border-gray-100 border-2 text-sm text-gray-100 
            rounded-3xl font-extrabold py-3 px-5 bg-transparent">
            Find out More
            </a>
        </div>
    </div>

    <div>
        <img src="https://cdn.pixabay.com/photo/2014/12/15/14/05/home-office-569153_960_720.jpg" alt="laptop image" >
    </div>

</div>
    
@endsection