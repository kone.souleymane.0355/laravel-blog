<footer class="bg-gray-700 py-10 mt-20">

    <div class="sm:grid grid-cols-3 pb-10 w-4/5  border-b-2 m-auto border-gray-600">

        <div>
            <h3 class="sm:font-bold text-gray-100 text-lg">
                Find Us
            </h3>

            <ul class="py-4 text-gray-400 sm:text-sm pt-4">

                <li class="pb-1"><a href="/">Home</a></li>
                <li class="pb-1"><a href="/blog">Blog</a></li>
                <li class="pb-1"><a href="/login">Login</a></li>
                <li class="pb-1"><a href="/register">Register</a></li>
            </ul>
        </div>

        <div>
            <h3 class="sm:font-bold text-gray-100 text-lg">
                Pages
            </h3>

            <ul class="py-4 text-gray-400 sm:text-sm pt-4">

                <li class="pb-1"><a href="/">What we do</a></li>
                <li class="pb-1"><a href="/">Adress</a></li>
                <li class="pb-1"><a href="/">Phone</a></li>
                <li class="pb-1"><a href="/">Contact</a></li>

            </ul>
        </div>

        <div>
            <h3 class="sm:font-bold text-gray-100 text-lg">
                Lastest Posts
            </h3>

            <ul class="py-4 text-gray-400 sm:text-sm pt-4">

                <li class="pb-1"><a href="/">Why leaning PHP</a></li>
                <li class="pb-1"><a href="/">Why Laravel is the best</a></li>
                <li class="pb-1"><a href="/">Why do you do that</a></li>
                <li class="pb-1"><a href="/">What do you do</a></li>

            </ul>
            
        </div>

    </div>
    <p class="text-xs  w-25 w-4/5 m-auto pt-3">
        Copyright 2021 Code in Laravel . All rights reserved.
    </p>
</footer>