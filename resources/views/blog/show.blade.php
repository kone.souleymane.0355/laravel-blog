@extends('layouts.app')

@section('content')
    
<div class="m-auto w-4/5 text-left">

    <div class="py-15">

        <h1 class="text-6xl ">{{ $post->title }}</h1>
    </div>

    <span class="text-gray-500">By 
        <span class="font-bold text-gray-300 italic">
            {{ $post->user->name }}
        </span>,Created on {{ date('jS M Y',strtotime($post->updated_at)) }}.
    </span>

    <p class="text-gray-700 pt-10 font-light pb-8 text-xl leading-8">
        {{ $post->description }}
    </p>
</div>


@endsection