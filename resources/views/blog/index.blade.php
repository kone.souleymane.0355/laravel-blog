@extends('layouts.app')

@section('content')

    <div class="m-auto w-4/5 text-center">

        <div class="py-15 border-b-2 border-gray-200">

            <h1 class="text-6xl ">Blogs posts</h1>
        </div>

    </div>

    @if (session()->has('success'))
        <div class="w-4/5 m-auto mt-10">

            <div class="w-2/5 bg-green-500 text-center font-bold text-gray-100 rounded-2xl text-2xl m-auto py-3 px-2">
                {{ session()->get('success') }}
            </div>
        </div>
    @endif

    @if (Auth::check())

        <div class="pt-8 w-4/5 m-auto">
            <a href="/blog/create" class="uppercase text-xs text-gray-100 bg-blue-500 bg-transparent
            py-2 px-5 rounded-3xl font-extrabold">
                Create a post
            </a>
        </div>
        
    @endif

    @foreach ($posts as $post )
        <div class="w-4/5 mx-auto sm:grid grid-cols-2 gap-20 py-10 border-b-2 border-gray-200">

            <div>

                <img src="{{ asset('images/'.$post->image_path) }}" alt="{{ $post->title }} image" >
        
            </div>
        
            <div>
                
                    
                
                <h2 class="text-4xl text-gray-400 font-bold  pb-4">
                    {{ $post->title }}
                </h2>

                <span class="text-gray-500">By 
                    <span class="font-bold text-gray-300 italic">
                        {{ $post->user->name }}
                    </span>,Created on {{ date('jS M Y',strtotime($post->updated_at)) }}.
                </span>

                <p class="text-gray-700 pt-10 font-light pb-8 text-xl leading-8">
                    {{ $post->description }}
                </p>
                <a href="/blog/{{ $post->slug }}" class="outline-none rounded-3xl bg-blue-500 px-5 py-2 font-extrabold text-gray-200
                text-xs uppercase"> Keep reading</a>
            

                @if (isset(Auth::user()->id) && Auth::user()->id == $post->user_id)

                    <a href="/blog/{{ $post->slug }}/edit" class="rounded-3xl mt-5 bg-yellow-200 px-8 py-2 font-extrabold text-gray-500
                        text-xs uppercase outline-none"> Edit post</a> 
                    <form action="/blog/{{ $post->slug }}" method="post" class="inline outline-none">

                        @csrf
                        @method('delete')
                        <button type="submit" class="rounded-3xl mt-5 bg-red-500 px-8 py-2 font-extrabold text-white
                        text-xs uppercase outline-none">Delete post</button>
                    
                    </form>
                                           
                @endif
                
            </div>

        </div>

        @endforeach
    
@endsection
