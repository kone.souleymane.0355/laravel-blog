@extends('layouts.app')

@section('content')
    
    <div class="m-auto w-4/5 text-left">

        <div class="py-15">

            <h1 class="text-6xl ">Create a post</h1>
        </div>
    </div>

    <div class="w-4/5 ">
        <ul class="bg-red-500 text-2xl w-2/5 rounded-3xl m-auto ">
            @if ($errors->any())
                @foreach ($errors->all() as $error )
                    <li class="text-gray-200 py-3 px-5 ">{{ $error }}</li>
                @endforeach
            @endif
        </ul>
    </div>

    <div class="w-4/5 m-auto pt-20">

        <form action="/blog" method="post" enctype="multipart/form-data" class="w-4/5 m-auto">
            
            @csrf

            <input type="text" name="title"  placeholder="Title.."
            class="w-full placeholder-gray-500 border-b-2 border-gray-400 bg-transparent h-20 text-6xl outline-none">

            <textarea name="description" placeholder="Description.."
            class="placeholder-gray-500   w-full py-20 bg-transparent h-60 text-xl
            outline-none border-b-2 border-gray-400"></textarea>

            <div class="sm:grid grid-cols-2 gap-36 w-4/5 m-auto ">
                <div class="bg-transparent pt-8 mx-5">

                    <label class="flex flex-col w-44 items-center bg-white shadow-xl border
                    border-blue-500 tracking-wide cursor-pointer">
                
                        <span class=" text-base leading-normal uppercase px-3 py-2">
                            Select a file
                        </span>
                        <input type="file" class="hidden" name="image">
                    </label>
    
                </div>
    
                <button type="submit" class="bg-white shadow-xl border
                border-blue-500 tracking-wide cursor-pointer  uppercase mt-10 mx-5 px-3  py-3 text-lg font-normal text-black">Post it</button>
            </div>
        </form>
    </div>
@endsection