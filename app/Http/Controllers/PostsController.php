<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Posts;
use \Cviebrock\EloquentSluggable\Services\SlugService;

class PostsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth',['except'=>['index','show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('blog.index')->with('posts',Posts::orderBy('updated_at','DESC')
                                                ->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'title'=>'required',
            'description'=>'required',
            'image'=>'required|mimes:png,jpg,jpeg|max:5048'
        ]);

        $slug = SlugService::createSlug(Posts::class,'slug',$request->title);

        $newImageName = uniqid(). '-' .$slug.'.' .$request->image->extension();
        
        $request->image->move(public_path('images'),$newImageName);

        Posts::create([

            'title'=>$request->input('title'),
            'slug'=>$slug,
            'description'=>$request->input('description'),
            'image_path'=>$newImageName,
            'user_id'=>auth()->user()->id
        ]);

        return redirect('/blog')->with('message','your post has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        return view('blog.show')->with('post',Posts::where('slug',$slug)->first());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        return view('blog.edit')->with('post',Posts::where('slug',$slug)->first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $request->validate([

            'title'=>'required',
            'description'=>'required',
            'image'=>'required|mimes:png,jpg,jpeg|max:5048'
        ]);

        $slug_ = SlugService::createSlug(Posts::class,'slug',$request->title);

        $newImageName = uniqid(). '-' .$slug_.'.' .$request->image->extension();
        
        $request->image->move(public_path('images'),$newImageName);

        Posts::where('slug',$slug)
                ->update([

            'title'=>$request->input('title'),
            'slug'=>$slug_,
            'description'=>$request->input('description'),
            'image_path'=>$newImageName,
            'user_id'=>auth()->user()->id
        ]);

        return redirect('/blog')->with('message','your post has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $post = Posts::where('slug',$slug);

        $post->delete();

        return redirect('/blog')->with('message','your post has been delected.');
    }
}
APP_NAME=NEFA
APP_ENV=local
APP_KEY=base64:m4+gOmz7PWUQBs29Asz3lF90BWNmSqDAV+nBacbhuuI=
APP_DEBUG=true
APP_URL=http://remote-course.service-sante.inphb.ci

LOG_CHANNEL=stack
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=services_formationlearn2
DB_USERNAME=services_formation
DB_PASSWORD=j'aiOublie12

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=mailhog
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
